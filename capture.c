#include <stdlib.h>
#include <stdio.h>
#include <error.h>
#include "camera.h"

static void handle_frame(MMAL_BUFFER_HEADER_T *buffer, void *context) {
	if (buffer->flags & MMAL_BUFFER_HEADER_FLAG_CONFIG) {
		fwrite(buffer->data, 1, buffer->length, stdout);
	} else if (buffer->flags & MMAL_BUFFER_HEADER_FLAG_CODECSIDEINFO) {
	
	} else {
		if (buffer->length) {
			mmal_buffer_header_mem_lock(buffer);
			fwrite(buffer->data, 1, buffer->length, stdout);
			mmal_buffer_header_mem_unlock(buffer);
		}
	}
}

int main(int argc, char **argv) {
	bcm_host_init();
	
	CameraState state = {
	  .width = 1792, // There are some weird limitations on the size of the stereoscopic video
	  .height = 1080,
	  .bitrate = 17000000,
	  .device_id = 0,
	  .stereoscopic = true
	};

	if (!camera_setup_capture(&state)) {
		error(EXIT_FAILURE, 0, "Failed to setup video capture");
	}
	
	run_frame_handler(&state, handle_frame, NULL);
	
	return EXIT_SUCCESS;
}


