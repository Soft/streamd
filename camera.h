#ifndef CAMERA_H
#define CAMERA_H

#include <stdint.h>
#include <stdbool.h>

#pragma GCC diagnostic ignored "-pedantic"
#pragma GCC diagnostic ignored "-Wvariadic-macros"
#include "bcm_host.h"
#include "interface/vcos/vcos.h"
#include "interface/mmal/mmal.h"
#include "interface/mmal/mmal_buffer.h"
#include "interface/mmal/mmal_logging.h"
#include "interface/mmal/util/mmal_util.h"
#include "interface/mmal/util/mmal_util_params.h"
#include "interface/mmal/util/mmal_default_components.h"
#include "interface/mmal/util/mmal_connection.h"
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

typedef struct {
	uint32_t device_id, width, height, bitrate;
	MMAL_COMPONENT_T *camera, *encoder;
	MMAL_CONNECTION_T *camera_encoder_conn;
	MMAL_POOL_T *pool;
	MMAL_QUEUE_T *decoded;
	bool stereoscopic;
} CameraState;

typedef void (*BufferHandler)(MMAL_BUFFER_HEADER_T *, void *);

bool camera_setup_capture(CameraState *);
void run_frame_handler(CameraState *, BufferHandler, void *);
void camera_state_free(CameraState *);

#endif

