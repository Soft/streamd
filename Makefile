CC = gcc
VC = /opt/vc
INCLUDES = -I$(VC)/include -I$(VC)/include/interface/vcos/pthreads -I$(VC)/include/interface/vmcs_host/linux
VCOS_LDFLAGS = -L$(VC)/lib -lmmal -lmmal_core -lmmal_util -lbcm_host -lvcos -lpthread
override CFLAGS += -std=gnu1x -Wall -Wextra -pedantic -fgnu89-inline
override LDFLAGS += $(VCOS_LDFLAGS) $(AVFORMAT_LDFLAGS)
DEBUG_CFLAGS = -g -Werror -DDEBUG
OBJS = camera.o capture.o

all: capture

debug: CFLAGS += $(DEBUG_CFLAGS)
debug: all

capture: $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<

.PHONY: clean
clean:
	for obj in $(OBJS); do rm -f "$$obj"; done
	-rm -f *.o capture

