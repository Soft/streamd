#include <error.h>
#include "camera.h"

static const size_t VIDEO_PORT = 1;

static bool camera_create_component(CameraState *);
static bool encoder_create_component(CameraState *);
static bool connection_create(MMAL_PORT_T *, MMAL_PORT_T *, MMAL_CONNECTION_T **);
static void control_callback(MMAL_PORT_T *, MMAL_BUFFER_HEADER_T *);
static void encoder_callback(MMAL_PORT_T *, MMAL_BUFFER_HEADER_T *);

#define MIN(a, b) ((a) < (b) ? (a) : (b))

static bool camera_create_component(CameraState *state) {
	MMAL_COMPONENT_T *camera = NULL;
	MMAL_PORT_T *port = NULL;
	MMAL_ES_FORMAT_T *format = NULL;
	MMAL_PARAMETER_INT32_T camera_num = {
		{ MMAL_PARAMETER_CAMERA_NUM, sizeof(camera_num) }, state->device_id
	};
	MMAL_PARAMETER_INT32_T sensor_mode = {
		{ MMAL_PARAMETER_CAMERA_CUSTOM_SENSOR_CONFIG, sizeof(sensor_mode) }, 0
	};
	MMAL_PARAMETER_CAMERA_CONFIG_T camera_config = {
		{ MMAL_PARAMETER_CAMERA_CONFIG, sizeof(camera_config) },
		.use_stc_timestamp = MMAL_PARAM_TIMESTAMP_MODE_RESET_STC,
		.stills_yuv422 = 0,
		.stills_capture_circular_buffer_height = 0,
		.one_shot_stills = 0,
		.num_preview_video_frames = 3,
		.max_stills_w = state->width,
		.max_stills_h = state->height,
		.max_preview_video_w = state->width,
		.max_preview_video_h = state->height,
		.fast_preview_resume = 0
	};
	MMAL_PARAMETER_STEREOSCOPIC_MODE_T stereoscopy_config = {
		{ MMAL_PARAMETER_STEREOSCOPIC_MODE, sizeof(stereoscopy_config)},
		.mode = MMAL_STEREOSCOPIC_MODE_SIDE_BY_SIDE,
		.decimate = true,
		.swap_eyes = false
	};
	if (mmal_component_create(MMAL_COMPONENT_DEFAULT_CAMERA, &camera) != MMAL_SUCCESS) {
		error(0, 0, "Could not create camera");
		goto release;
	}
	port = camera->output[VIDEO_PORT];
	if (state->stereoscopic) {
		if (mmal_port_parameter_set(port, &stereoscopy_config.hdr) != MMAL_SUCCESS) {
			error(0, 0, "Could not enable stereoscopic mode");
			goto release;
		}
	}
	if (mmal_port_parameter_set(camera->control, &camera_num.hdr) != MMAL_SUCCESS) {
		error(0, 0, "Could not select camera");
		goto release;
	}
	if (mmal_port_parameter_set(camera->control, &sensor_mode.hdr) != MMAL_SUCCESS) {
		error(0, 0, "Could not set sensor mode");
		goto release;
	}
	if (mmal_port_enable(camera->control, control_callback) != MMAL_SUCCESS) {
		error(0, 0, "Could not enable camera");
		goto release;
	}
	if (mmal_port_parameter_set(camera->control, &camera_config.hdr) != MMAL_SUCCESS) {
		error(0, 0, "Could not configure camera");
		goto release;
	}

	format = port->format;
	format->encoding = MMAL_ENCODING_OPAQUE;
	format->encoding_variant = MMAL_ENCODING_I420;
	format->es->video.width = VCOS_ALIGN_UP(state->width, 32);
	format->es->video.height = VCOS_ALIGN_UP(state->height, 16);
	format->es->video.frame_rate.num = 0;
	format->es->video.frame_rate.den = 1;
	format->es->video.crop.y = 0;
	format->es->video.crop.x = 0;
	format->es->video.crop.width = state->width;
	format->es->video.crop.height = state->height;

	if (mmal_port_format_commit(port) != MMAL_SUCCESS) {
		vcos_log_error("Could not set camera format");
		goto release;
	}

	port->buffer_num = 3;

	if (mmal_component_enable(camera) != MMAL_SUCCESS) {
		error(0, 0, "Could not enable camera");
		goto release;
	}
	state->camera = camera;
	return true;
release:
	if (camera) {
		mmal_component_destroy(camera);
	}
	return false;
}

static bool encoder_create_component(CameraState *state) {
	MMAL_COMPONENT_T *encoder = NULL;
	MMAL_PORT_T *input = NULL, *output = NULL;
	MMAL_POOL_T *pool = NULL;
	MMAL_PARAMETER_VIDEO_PROFILE_T profile = {
		{ MMAL_PARAMETER_PROFILE, sizeof(profile) },
		{{ MMAL_VIDEO_PROFILE_H264_CONSTRAINED_BASELINE, MMAL_VIDEO_LEVEL_H264_4 }}
	};
	if (mmal_component_create(MMAL_COMPONENT_DEFAULT_VIDEO_ENCODER, &encoder) != MMAL_SUCCESS) {
		error(0, 0, "Could not create encoder");
		goto release;
	}
	input = encoder->input[0];
	output = encoder->output[0];
	mmal_format_copy(output->format, input->format);
	output->format->encoding = MMAL_ENCODING_H264;
	output->format->bitrate = state->bitrate;
	output->format->es->video.frame_rate.num = 0;
	output->format->es->video.frame_rate.den = 1;
	output->buffer_size = MIN(output->buffer_size_recommended, output->buffer_size_min);
	output->buffer_num = MIN(output->buffer_num_recommended, output->buffer_num_min);
	if (mmal_port_format_commit(output) != MMAL_SUCCESS) {
		error(0, 0, "Could not set encoder format");
		goto release;
	}
	if (mmal_port_parameter_set(output, &profile.hdr) != MMAL_SUCCESS) {
		error(0, 0, "Could not set video profile");
		goto release;
	}
	if (mmal_component_enable(encoder) != MMAL_SUCCESS) {
		error(0, 0, "Could not enable encoder");
		goto release;
	}
	if ((pool = mmal_pool_create(output->buffer_num, output->buffer_size)) == NULL) {
		error(0, 0, "Could not create pool");
		goto release;
	}
	state->encoder = encoder;
	state->pool = pool;
	return true;
release:
	if (encoder) {
		mmal_component_destroy(encoder);
	}
	return false;
}

static bool connection_create(MMAL_PORT_T *from, MMAL_PORT_T *to, MMAL_CONNECTION_T **conn) {
	MMAL_CONNECTION_T *connection = NULL;
	if (mmal_connection_create(&connection, from, to,
		MMAL_CONNECTION_FLAG_TUNNELLING | MMAL_CONNECTION_FLAG_ALLOCATION_ON_INPUT) != MMAL_SUCCESS) {
		goto release;
	}
	if (mmal_connection_enable(connection) != MMAL_SUCCESS) {
		goto release;
	}
	*conn = connection;
	return true;
release:
	if (connection) {
		mmal_connection_destroy(connection);
	}
	return false;
}

void camera_state_free(CameraState *state) {
	if (state->camera) {
		mmal_component_destroy(state->camera);
	}
	if (state->encoder) {
		mmal_component_destroy(state->encoder);
	}
	if (state->camera_encoder_conn) {
		mmal_connection_destroy(state->camera_encoder_conn);
	}
	if (state->decoded) {
		mmal_queue_destroy(state->decoded);
	}
	if (state->pool) {
		mmal_pool_destroy(state->pool);
	}
}

bool camera_setup_capture(CameraState *state) {
	MMAL_PORT_T *camera_out, *encoder_in, *encoder_out;
	if (!camera_create_component(state)) {
		goto release;
	}
	if (!encoder_create_component(state)) {
		goto release;
	}
	camera_out = state->camera->output[VIDEO_PORT];
	encoder_in = state->encoder->input[0];
	encoder_out = state->encoder->output[0];
	if (!connection_create(camera_out, encoder_in,
		&state->camera_encoder_conn)) {
		error(0, 0, "Could not connect components");
		goto release;
	}
	encoder_out->userdata = (struct MMAL_PORT_USERDATA_T *)state;
	if (mmal_port_enable(encoder_out, encoder_callback) != MMAL_SUCCESS) {
		error(0, 0, "Could not enable encoder output port");
		goto release;
	}

	state->decoded = mmal_queue_create();
	unsigned int buffers = mmal_queue_length(state->pool->queue);
	for (unsigned int i = 0; i < buffers; i++) {
		MMAL_BUFFER_HEADER_T *buffer = mmal_queue_get(state->pool->queue);
		if (!(buffer && (mmal_port_send_buffer(encoder_out, buffer) == MMAL_SUCCESS))) {
			error(0, 0, "Could not send buffer to encoder");
		}
	}

	if (mmal_port_parameter_set_boolean(camera_out, MMAL_PARAMETER_CAPTURE, 1) != MMAL_SUCCESS) {
		error(0, 0, "Failed to start capture");
		goto release;
	}

	return true;
release:
	camera_state_free(state);
	return false;
}

void run_frame_handler(CameraState *state, BufferHandler handler, void *data) {
	MMAL_BUFFER_HEADER_T *buffer = NULL;
	MMAL_PORT_T *encoder_out = state->encoder->output[0];
	while (true) {
		if ((buffer = mmal_queue_wait(state->decoded)) != NULL) {
			handler(buffer, data);
			mmal_buffer_header_release(buffer);
			if (encoder_out->is_enabled) {
				buffer = mmal_queue_get(state->pool->queue);
				if (!(buffer && (mmal_port_send_buffer(encoder_out, buffer) == MMAL_SUCCESS))) {
					error(0, 0, "Could not return buffer to encoder");
				}
			}
		}
	}
}

static void control_callback(MMAL_PORT_T *port, MMAL_BUFFER_HEADER_T *buffer) {
	(void)port;
	mmal_buffer_header_release(buffer);
}

static void encoder_callback(MMAL_PORT_T *port, MMAL_BUFFER_HEADER_T *buffer) {
	CameraState *state = (CameraState *)port->userdata;
	if (!state) {
		error(0, 0, "Encoder did not transmit context");
		mmal_buffer_header_release(buffer);
		return;
	}
	if (buffer->flags & MMAL_BUFFER_HEADER_FLAG_FRAME_END) {
		vcos_log_info("Frame completed");
	}
	if (buffer->flags & MMAL_BUFFER_HEADER_FLAG_TRANSMISSION_FAILED) {
		vcos_log_info("Transmission failed");
	}
	mmal_queue_put(state->decoded, buffer);
}


